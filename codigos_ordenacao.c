#include <stdio.h>
#include <stdbool.h>
#include <string.h>
                    
#define vTam(vet) ( sizeof(vet) / sizeof(vet[0]) )

//para simplificação, toda string será um vetor de 63 chars + '\0'
typedef char string[64];

void trocar(int* a, int* b) {
	int temp = *a;
	*a = *b;
	*b = temp;
}
    
bool menorOuIgual(int a, int b) {
	return a <= b;
}

//imprime os elementos de um vetor,
//de 0 até tam - 1
void imprimirVetor(int vet[], int tam) {
	for (int i = 0; i < tam; ++i) {
		printf("%d ", vet[i]);
	}
	printf("\n");
}

//"insere" o elemento da posição i
//em sua posição correta no vetor
void insercao (int vet [], int p, int i) {
	int el = vet[i];
	for (; i > p && el < vet[i-1]; --i) {
			vet[i] = vet[i-1];
	}
	vet[i] = el;
}

//ordena por inserção os elementos do vetor nas posições
//de p até u (incluindo o elemento na posição u)
void ordenaInsercao(int vet[], int p, int u) {
	for (int i = p+1; i <= u; i++) {
		int el = vet[i];
		int j = i;
		for (; j > p && el < vet[j-1]; --j) {
				vet[j] = vet[j-1];
		}
		vet[j] = el;
	}
}

//utiliza meio, isso é: p + (u - p)/2, como pivo
//retorna o indice final do pivo (que estará na posição correta)
int particionar(int vet[], int p, int u) {
	//uma estrategia é trocar o elemento do meio com o primeiro elemento
	//e então particionar usando o algoritmo que usa o primeiro como pivô
	int meio = p + (u - p)/2;
	trocar(&vet[meio], &vet[p]);
	int pivo = vet[p]; //pivo da partição
	int bMa = p; //indice que busca primeiro maior que pivo
	int bMe = u+1;  //indice que busca primeiro menor_ou_igual a pivo
	
	while(true) {
		//bMa inicia no começo do vetor   
		while(menorOuIgual(vet[++bMa], pivo)) { //busca primeiro > pivo
			if (bMa == u) break; //garantir que indice não vá além do fim do intervalo
		} 
		//bMa aponta para primeiro elemento > pivo

		//bMe inicia no fim do vetor
		while (!menorOuIgual(vet[--bMe], pivo)) { //busca primeiro <= pivo
			if (bMe == p) break; //garantir que indice não vá além do começo do intervalo
		} 
		//bMe aponta para primeiro elento <= pivo

		if (bMa >= bMe) break; //indices cruzaram-se!
		trocar(&vet[bMa], &vet[bMe]); //trocamos enquanto indices não se cruzarem
	}
	//Colocamos o pivo na posição final dele:
	trocar(&vet[p], &vet[bMe]); //a direita de bMe todos são maiores que pivo
	return bMe; //retorna-se o indice final do pivo
}

//ordena por partição os elementos do vetor nas posições
//de p até u (incluindo o elemento na posição u)
void ordenaParticao(int vet[], int p, int u) {
	if (u - p  < 2) {
		return;
	} 

	int meio = p + (u - p)/2;
	trocar(&vet[meio], &vet[p]);
	int pivo = vet[p]; //pivo da partição
	int bMa = p; //indice que busca primeiro maior que pivo
	int bMe = u+1;  //indice que busca primeiro menor_ou_igual a pivo
	
	while(true) {
		//bMa inicia no começo do vetor   
		while(menorOuIgual(vet[++bMa], pivo)) { //busca primeiro > pivo
			if (bMa == u) break; //garantir que indice não vá além do fim do intervalo
		} 
		//bMa aponta para primeiro elemento > pivo

		//bMe inicia no fim do vetor
		while (!menorOuIgual(vet[--bMe], pivo)) { //busca primeiro <= pivo
			if (bMe == p) break; //garantir que indice não vá além do começo do intervalo
		} 
		//bMe aponta para primeiro elento <= pivo

		if (bMa >= bMe) break; //indices cruzaram-se!
		trocar(&vet[bMa], &vet[bMe]); //trocamos enquanto indices não se cruzarem
	}
	//Colocamos o pivo na posição final dele:
	trocar(&vet[p], &vet[bMe]); //a direita de bMe todos são maiores que pivo
	int indice_pivo = bMe; //o indice final do pivo


	ordenaParticao(vet, p, indice_pivo-1); //ordene metade à esquerda do pivo
	ordenaParticao(vet, indice_pivo+1, u); //ordene metade à direita do pivo
}

//Realiza uma busca binária do elemento de valor agulha
//na sequência de elementos nas posições de p a u do vetor
//retorna -1 caso o elemento não esteja nesse intervalo
int buscaBin(int agulha, int vet[], 
			 int p, int u) {
    int m;

    while ( p <= u ) { //enquanto houver intervalo [p ... u]
    	m = (p + u)/2;
    	
    	if (agulha == vet[m]) {
    		return m;
    	} else if (agulha > vet[m]) {
    		p = m + 1; 
    	} else { 
    		u = m - 1; 
    	}
    }

    return -1; //não encontrado
}


int main () {

	int vetor[] = {9, 22, 3, 1, 5, 15, 22, 48, 52, 33, 9, 4, 31};

	ordenaParticao(vetor, 0, vTam(vetor) - 1);

	imprimirVetor(vetor, vTam(vetor));

}